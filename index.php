<?php

require_once('animal.php'); //requireonce pernah diambil dgn nama yg sama maka yg berikutnya dibatalkan
require_once('frog.php');
require_once('ape.php');

$sheep = new Animal("shaun");
echo "Name: ". $sheep->hewan . "<br>";
echo "Legs: ". $sheep->legs . "<br>";
echo "Cold blooded: ". $sheep->cold_blood . "<br>","<br>";

$kodok = new Frog("buduk");
echo "Name: ". $kodok->hewan . "<br>";
echo "Legs: ". $kodok->legs . "<br>";
echo "Cold blooded: ". $kodok->cold_blood . "<br>";
echo "Yell: ". $kodok->jump . "<br>","<br>";

$sungokong = new Ape("kera sakti");
echo "Name: ". $sungokong->hewan . "<br>";
echo "Legs: ". $sungokong->legs . "<br>";
echo "Cold blooded: ". $sungokong->cold_blood . "<br>";
echo "Yell: ". $sungokong->yell . "<br>","<br>";

?>